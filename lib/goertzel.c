#include <math.h>
#include <stdint.h>

float goertzel(int16_t *samples, int nSamples, float freq, int sampleRate) {
  int k = 0.5 + (nSamples*freq)/sampleRate;
  float w = ( (2 * M_PI) / nSamples) * k;
  float coeff = 2 * cos(w);

  double q0, q1, q2;
  q0 = q1 = q2 = 0.0;
  int i;
  for (i = 0; i < nSamples; i++) {
    q0 = coeff*q1 - q2 + samples[i];
    q2 = q1;
    q1 = q0;
  }

  float power = q1*q1 + q2*q2 - q1*q2*coeff;
  double magnitude = sqrt(power) / nSamples;
  return magnitude;
}

#include <stdio.h>

float testSO(int a, float b) {
  printf("a: %d, b: %f\n", a, b);
  return a + b;
}

