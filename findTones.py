import sys
import math
import multiprocessing
import multiprocessing.sharedctypes

import numpy as np
import matplotlib.pyplot as plt

from scipy import signal, fftpack
from scipy.io import wavfile

import goertzel

import time

# freq = 20000	#Hz
# pulseLength = 1 #sec
# blockSize = int(sys.argv[2]) if len(sys.argv) == 3 else 64	#optimal: 64 or 128?
# threshold = 0.6
# lengthError = 0.1 #sec
# medianKernelLength = 0.2 #sec (how far on each side to look while median filtering)

def windowedGoertzelMultiprocessWrapper(destArr, data, freq, rate, blockSize, printProgress=False):
	mag = goertzel.windowedGoertzel(data, freq, rate, blockSize= blockSize, printProgress= printProgress, dest= destArr)
	if printProgress: print 'Finished measuring {} Hz noise'.format(freq)

def findToneStarts(data, rate, freq, noiseFreq=None, pulseLength=1, blockSize=128, lengthError=0.1, medianKernelLength=0.2, threshold=2, printGraphs=True, printProgress= True):
	# calculate frequency's power in each block
	start = time.clock()
	multiprocess = True
	if multiprocess:
		if noiseFreq is not None:
			# Creating a C-array in shared memory is fastest; Pipes and Queues can't transfer large amounts of data,
			# leading to dead hang as the subprocess waits for the Pipe to transfer, and the main process waits for the subprocess to end
			noise = multiprocessing.sharedctypes.RawArray('f', int(math.ceil(data.size/float(blockSize))))
			noise = np.frombuffer(noise, np.dtype('f'))

			noise_proc = multiprocessing.Process(target= windowedGoertzelMultiprocessWrapper, args= (noise, data, noiseFreq, rate, blockSize))
			noise_proc.start()
			
			magnitude = goertzel.windowedGoertzel(data, freq, rate, blockSize=blockSize, printProgress= printProgress)
			# if printProgress: print 'Finished measuring {} Hz signal, waiting for noiseprint to finish...'.format(freq)
			noise_proc.join()
			# if printProgress: print '...done. Filtering and analyzing signal...'
		else:
			magnitude = goertzel.windowedGoertzel(data, freq, rate, blockSize=blockSize, printProgress= printProgress)
	else:
		magnitude = goertzel.windowedGoertzel(data, freq, rate, blockSize=blockSize, printProgress= printProgress)
		if noiseFreq is not None:
			noise = goertzel.windowedGoertzel(data, noiseFreq, rate, blockSize=blockSize, printProgress= printProgress)
	end = time.clock()
	# if printProgress: print 'Multiprocess is {}: executed in {:.3f} seconds'.format('on' if multiprocess else 'off', end - start)

	gRate = rate/blockSize

	# smooth power graph by median filtering
	kernelSize = int(gRate * medianKernelLength)
	if kernelSize % 2 == 0:
		kernelSize += 1 		# kernel size must be odd
	mag_median = signal.medfilt(magnitude, kernel_size= kernelSize)
	if noiseFreq is not None:
		noise_median = signal.medfilt(noise, kernel_size= kernelSize)
		mag_median = mag_median - noise_median
		mag_median[ mag_median < 0 ] = 0
	# normalize power graph
	mean = np.mean(mag_median)
	meansMean = np.ma.mean(mag_median[ mag_median < mean ]) # mean of all points below the mean
	mag_median = mag_median / mean

	# binarize power graph
	thresh = [1 if x >= threshold else 0 for x in mag_median]	# why is this faster
	# thresh = mag_median >= threshold							# than this?

	# calculate sum of next binarized points pulseLength ahead
	window = int(pulseLength * gRate)
	running_sum = np.array([ sum(thresh[i:i+window]) for i in xrange(0, len(thresh) - window) ]) # TODO: improve speed by memoization
	if printGraphs is not False: unthreshed = running_sum.copy()
	# threshold sum to only pass pulses of sufficient length (pulseLength - lengthError)
	# TODO: filter pulses that are too long?
	minLength = int(pulseLength*gRate - lengthError*gRate)
	running_sum = [x if x >= minLength else 0 for x in running_sum]
	deriv = np.diff(running_sum)

	# print '{}, {}sec'.format(np.argmax(running_sum), np.argmax(running_sum) * (1.0/gRate))

	# find all points where derivative switches from positive to negative - these will be positive peaks / tone starts
	# (because of the binarization, all corners are sharp with little to no noise)
	starts = []
	ends = []
	thisPeakOver = False
	for i in xrange(deriv.size - 1):
		if not thisPeakOver:
			if deriv[i] > 0 and deriv[i+1] <= 0:
				starts.append(i+1)
				thisPeakOver = True
		else:
			if deriv[i] < 0:		# "debounce" small dips below the threshold to prevent double-registering a peak a few points apart
				thisPeakOver = False
				ends.append( i + running_sum[i] )
	# TODO: how to handle case when sum is too long (has flat peak)? depending on situation, beginning or end of peak will be correct (see 1_end vs faint)

	# refine starts and ends
	dx = np.diff(mag_median)

	refinedStarts = []
	for s in starts:
		while s > len(dx) and dx[s] > 0:
			s -= 1
		refinedStarts.append(s)
	refinedEnds = []
	for e in ends:
		while e < len(dx) and dx[e] < 0:
			e += 1
		refinedEnds.append(e)

	## Printing
	if printGraphs == 'foundOnly':
		if len(starts) > 0:
			printGraphs = True
		else:
			printGraphs = False

	if printGraphs is True:
		fig, axs = plt.subplots(5, 1, sharex= False)
		axs[0].plot(mag_median)
		# if noiseFreq is not None:
		axs[0].plot( (magnitude - noise_median) / mean, color= 'yellow')
		axs[0].set_title('Magnitude of {} Hz'.format(freq))
		axs[0].axhline(y= threshold, color='r')

		axs[0].axhline(y= mean / mean, color= 'pink')
		axs[0].axhline(y= meansMean / float(mean), color= 'purple')
		axs[0].axhline(y= np.mean(magnitude - noise_median) / float(mean), color= 'orange')

		axs[1].set_title('Derivative of magnitude')
		axs[1].plot(dx)

		axs[2].plot(thresh)
		axs[2].set_title('Binarized magnitude')
		axs[3].plot(running_sum)
		axs[3].plot(unthreshed, color= 'r')
		axs[3].axhline(y= minLength)
		axs[3].axhline(y= pulseLength*gRate)
		axs[3].set_title('Thresholded running sum')
		axs[4].plot(deriv)
		axs[4].set_title('Derivative of running sum')

		plt.xticks( np.linspace(0, mag_median.size, 25), np.linspace(0, len(data)/rate, 25) )

		for s, r_s in zip(starts, refinedStarts):
			for ax in axs:
				ax.axvline(x= s, color='g')
				ax.axvline(x= r_s, color='olive')
		for e, r_e in zip(ends, refinedEnds):
			for ax in axs:
				ax.axvline(x= e, color='orange')
				ax.axvline(x= r_e, color='maroon')
		plt.show()
	return np.array(starts) * (1.0/gRate)	# tone starts in seconds

def toneSearch(data, rate, freq, noiseFreq=None, pulseLength=1):
	rough = findToneStarts(data, rate, freq, noiseFreq= noiseFreq, blockSize= (rate*pulseLength)/4, lengthError= 0.2, threshold= 1.9, printGraphs= False, printProgress= True)
	# print 'Found {} possible tones, enhancing results with short-windowed Goertzel filter.'.format(len(rough))
	searchWindow = 3 #seconds around each rough timestamp to search (1/3 before, 2/3 after)
	searchSamples = searchWindow * rate
	times = []
	for i, t in enumerate(rough):
		sample = t * rate
		start = int(sample-(searchSamples/3.0))
		end = int(sample+(searchSamples*2/3.0))
		window = data[start : end]
		time = findToneStarts(window, rate, freq, noiseFreq= noiseFreq, blockSize= 128, lengthError= 0.15, medianKernelLength= 0.15, threshold= 1, printGraphs= False, printProgress= False)
		if len(time) != 1:
			pass
			# print '{}/{}: invalid (found {} tones from {:.3f} to {:.3f} sec)'.format(i+1, len(rough), len(time), start/float(rate), end/float(rate))
		else:
			fullTime = time[0] + start/float(rate)
			times.append(fullTime)
			# print '{}/{}: {:.3f} to {} sec valid ({:.3f} sec)'.format(i+1, len(rough), start/float(rate), end/float(rate), fullTime)
	return times


if __name__ == "__main__":
  if len(sys.argv) < 2 or len(sys.argv) > 4:
    print 'Useage: {} [target_frequency] [reference_frequency] filename'.format(sys.argv[0])
  else:
    filename = sys.argv[-1]
    print 'Reading file...'
    rate, data = wavfile.read(filename)
    if data.ndim > 1:
      data = data[:, 0] # just use 1 channel
    print '...done.'
    targetFreq, refFreq = 20000, 19000
    if len(sys.argv) > 2:
      targetFreq = int(sys.argv[1])
    if len(sys.argv) > 3:
      refFreq = int(sys.argv[2])
    times = toneSearch(data, rate, targetFreq, refFreq, pulseLength= 1)
    print '***************'
    print 'Found {} tones:'.format(len(times))
    print times


    # times = findToneStarts(data, rate, 20000, blockSize= 128, lengthError= 0.3, threshold= 2)
    # print times
