import math
import numpy as np
import sys

def windowedGoertzel(samples, freq, sampleRate, resolution=100, blockSize=None, printProgress=True, dest=None):
	"""
	Calculates the magnitude of the specified frequency over time in the sample.
	The magnitude will be calculated within 'resolution' Hz of the target frequency (note that very
	fine resolutions may lead to degradation, as the window size increases beyond the length of the
	very signal you're looking for); or manually specifying blockSize will override the automatic calculation
	from resolution. Returns an array, where each element is the magnitude over blockSize samples in the input.
	"""
	if blockSize is None:
		target_block = sampleRate/float(resolution)
		blockSize = int(round(target_block))
		# print "Block size: {}".format(blockSize)
	blockSize = int(blockSize)
	# if printProgress: print 'Resolution: {:.2f} Hz'.format(sampleRate/float(blockSize))
	
	targetSize = math.ceil(samples.size/float(blockSize))
	if dest is None:
		result = np.empty(targetSize, dtype= np.float)
	else:
		assert len(dest) >= targetSize, \
		'Destination array too small to fit Goertzel results (length: {}, must be at least {} elements)'.format(len(dest), targetSize)
		result = np.asarray(dest, dtype= np.float)

	for i in xrange(0, samples.size, blockSize):
		if i+blockSize > samples.size:
			block = samples[i:]
		else:
			block = samples[i:i+blockSize]
		if printProgress: sys.stdout.write( "\rCalculating power of {} Hz: {:.1%}".format(freq, float(i)/samples.size) )
		mag = goertzel(block, freq, sampleRate)
		result[i/blockSize] = mag
		if printProgress: sys.stdout.flush()
	if printProgress: sys.stdout.write("\n")
	return result


import ctypes
from ctypes import cdll

import os

# Import the goertzel shared-object library
lib_g = cdll.LoadLibrary( os.path.join(os.path.dirname(__file__), 'lib/libgoertzel.dylib') )
lib_g.goertzel.restype = ctypes.c_float
lib_g.goertzel.argtypes = [ np.ctypeslib.ndpointer(dtype= np.dtype('<i2'), ndim= 1, flags= 'CONTIGUOUS'), ctypes.c_int, ctypes.c_float, ctypes.c_int ]

def goertzel(samples, freq, sampleRate):
	"""
	Returns the magnitude of the given frequency in the sample.

	samples should be a contiguous 1-d ndarray of 2-byte integers (for optimal performance, otherwise it will be converted)
	Wrapper for the libgoertzel.dylib dynamic library, which must be in the same directory.
	"""
	samples = np.asarray(samples, dtype= np.dtype('<i2'), order= 'C')
	assert samples.ndim == 1, "The Goertzel algorithm only runs on 1-dimensional (numpy) arrays, yours has {} dimensions".format(samples.ndim)
	mag = lib_g.goertzel( samples, samples.size, freq, sampleRate )
	return mag


###############################
# (MOSTLY) DEPRECATED FUNCTIONS

def goertzel_py(samples, freq, sampleRate):
	"""
	Returns the magnitude of the given frequency in the sample.
	"""
	size = len(samples)
	k = int( 0.5 + (size*freq)/sampleRate )
	w = ((2*math.pi)/size) * k
	cosine = math.cos(w)
	coeff = 2*cosine

	q0 = q1 = q2 = 0.0
	for x in samples:
		q0 = coeff*q1 - q2 + x
		q2 = q1
		q1 = q0

	magnitude = math.sqrt( q1**2 + q2**2 - q1*q2*coeff ) # TODO: return power, not magnitude (no sqrt)?
	# return magnitude
	return magnitude / size

def goertzel_weave(samples, freq, sampleRate):
	from scipy import weave
	"""
	Returns the magnitude of the given frequency in the sample. Speed is boosted by compiling the main loop in C++ (around 140x faster!).
	"""
	size = len(samples)
	k = int( 0.5 + (size*freq)/sampleRate )
	w = ((2*math.pi)/size) * k
	cosine = math.cos(w)
	coeff = 2*cosine


	samples = np.asarray(samples, dtype= np.float32)
	assert samples.ndim == 1, "The Goertzel algorithm only runs on 1-dimensional (numpy) arrays, yours has {} dimensions".format(samples.ndim)
	assert type(coeff) == type(1.0), "Coefficient must be a float! That's weird."
	loop = """
		#line 42 "goertzel.py"		// yup, this is line 41. I know. No idea why line counting is off in weave.
		#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION	// apparently necessary to squelch warnings; code doesn't use
															// deprecated API, but something compiled inside SciPy does.
															// Also, this doesn't make it go away.
		double q0, q1, q2;
		q0 = q1 = q2 = 0.0;

		int n = Nsamples[0];
		float x;	//this assumes the system uses 32bit floats. This should be changed to get the real type from the array.
		for (int i = 0; i < n; i++) {
			x = SAMPLES1(i);
			q0 = (coeff * q1) - q2 + x;
			q2 = q1;
			q1 = q0;
		}
		double power = q1*q1 + q2*q2 - q1*q2*coeff;
		double magnitude = sqrt(power) / n;
		return_val = magnitude;
	"""
	mag = weave.inline(loop, ['samples', 'coeff'], headers= ['<cmath>'])
	return mag
 